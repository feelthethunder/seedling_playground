import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_with_fire/models/user.dart';
import 'package:flutter_with_fire/services/database.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  //create user obj based on Firebase user.

  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uId: user.uid) : null;
  }

  Stream<User> get user {
    //return _auth.onAuthStateChanged.map((FirebaseUser firebaseUser) => _userFromFirebaseUser(firebaseUser));
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  //sign in anonymous
  Future signInAnonymously() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //sign in with email & password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //register with email & password
  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);

      await DatabaseService(uid: result.user.uid).updateUserData('0', 'new name', 100);
      return _userFromFirebaseUser(result.user);
    } catch (e) {
      e.toString();
      return null;
    }
  }

  //sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
