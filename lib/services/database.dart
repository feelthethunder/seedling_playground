import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {

  final String uid;
  DatabaseService({this.uid});

  // Collection reference
  //final CollectionReference collection = Firestore.instance.collection('table');
  final collection = Firestore.instance.collection('table');

  Future updateUserData(String sugars, String name, int strength) async {
    return await collection.document(uid).setData({
      "sugars" : sugars,
      "name" : name,
      "stregth" : strength
    });
  }

  Stream<QuerySnapshot> get brews {
    return collection.snapshots();
  }
}
